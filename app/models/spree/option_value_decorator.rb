Spree::OptionValue.class_eval do
  scope :search_with_products_and_presentation, -> (products, presentation) {
    where(option_type_id: Spree::OptionType.search_with_products_and_presentation(products, presentation).ids)
  }
end

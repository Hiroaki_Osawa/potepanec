Spree::Variant.class_eval do
  scope :has_option_value, -> (option_value_name) {
    joins(:option_values).where(spree_option_values: { name: option_value_name })
  }
  scope :with_image_and_price, -> { includes(:images, :prices) }
  scope :search_with_products, -> (products) { where(product_id: products.ids) }
  scope :order_descend_by_price, -> { joins(:prices).order("spree_prices.amount DESC") }
  scope :order_ascend_by_price, -> { joins(:prices).order("spree_prices.amount ASC") }
  scope :order_newest_by_product_available_on, -> { joins(:product).order("spree_products.available_on DESC") }
  scope :order_oldest_by_product_available_on, -> { joins(:product).order("spree_products.available_on ASC") }
  scope :order_by, ->(order) {
    case order
    when 'price-dsc' then order_descend_by_price
    when 'price-asc' then order_ascend_by_price
    when 'oldest'    then order_oldest_by_product_available_on
    else order_newest_by_product_available_on
    end
  }
end

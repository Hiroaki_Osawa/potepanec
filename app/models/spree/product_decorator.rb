Spree::Product.class_eval do
  scope :order_newest, -> { order(available_on: :desc) }
  scope :order_oldest, -> { order(available_on: :asc) }
  scope :with_master_image_and_price, -> { includes(master: [:images, :default_price]) }
  scope :taxons_relation_products, -> (product) {
    joins(:taxons).where(spree_taxons: { id: product.taxons.ids }).
      where.not(id: product.id).distinct
  }
  scope :has_option_value, -> (value) {
    joins(variants_including_master: :option_values).where(spree_option_values: { id: value.id }).distinct
  }
  scope :reorder_by, -> (order) {
    case order
    when 'price-dsc' then reorder(nil).descend_by_master_price
    when 'price-asc' then reorder(nil).ascend_by_master_price
    when 'oldest'    then reorder(nil).order_oldest
    else reorder(nil).order_newest
    end
  }

  def self.order_option_hash
    {
      "新着順":      'newest',
      "価格の安い順": 'price-asc',
      "価格の高い順": 'price-dsc',
      "古い順":      'oldest',
    }
  end
end

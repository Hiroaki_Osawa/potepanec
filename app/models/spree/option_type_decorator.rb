Spree::OptionType.class_eval do
  scope :search_with_products_and_presentation, -> (products, presentation) {
    joins(:products).
      where(presentation: presentation, spree_products: { id: products.ids })
  }
end

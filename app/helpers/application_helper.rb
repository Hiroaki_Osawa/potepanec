module ApplicationHelper
  def full_title(page_title = '')
    base_title = "BIGBAG Store"
    if page_title.empty?
      base_title
    else
      page_title + " - " + base_title
    end
  end

  def cover_all_title(*titles)
    titles.join(" ")
  end
end

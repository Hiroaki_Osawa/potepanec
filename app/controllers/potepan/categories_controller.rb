class Potepan::CategoriesController < ApplicationController
  def show
    @taxon = Spree::Taxon.find(params[:id])
    @taxonomies = Spree::Taxonomy.all.includes(:taxons)
    @same_taxon_products = Spree::Product.in_taxon(@taxon)
    @products =
      if params[:color]
        Spree::Variant.
          with_image_and_price.
          search_with_products(@same_taxon_products).
          has_option_value(params[:color]).
          order_by(params[:order]).
          uniq { |variant| variant.product_id }
      else
        @same_taxon_products.with_master_image_and_price.reorder_by(params[:order])
      end

    @color_option_values =
      Spree::OptionValue.
        search_with_products_and_presentation(@same_taxon_products, 'Color').
        uniq
  end
end

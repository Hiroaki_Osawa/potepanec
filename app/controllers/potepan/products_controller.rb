class Potepan::ProductsController < ApplicationController
  MAX_PRODUCT_SQL = 10
  MAX_RELATION_PRODUCTS_IN_PAGE = 4
  def show
    @product = Spree::Product.find(params[:id])
    @relation_products =
      Spree::Product.
        with_master_image_and_price.
        taxons_relation_products(@product).
        limit(MAX_PRODUCT_SQL).
        sample(MAX_RELATION_PRODUCTS_IN_PAGE)
  end
end

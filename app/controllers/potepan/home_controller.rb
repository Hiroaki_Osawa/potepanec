class Potepan::HomeController < ApplicationController
  MAX_NEW_PRODUCTS_IN_PAGE = 10
  def index
    @new_products = Spree::Product.with_master_image_and_price.order_newest.limit(MAX_NEW_PRODUCTS_IN_PAGE)
  end
end

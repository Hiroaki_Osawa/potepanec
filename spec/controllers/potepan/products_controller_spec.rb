require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  let(:product) { create(:product) }
  let(:bag_taxon) { create(:taxon, name: "bag") }
  let(:bag) { create(:product, name: 'bag') }
  let(:tote) { create(:product, name: 'tote') }

  describe "GET product show" do
    before do
      get :show, params: { id: product.id }
    end

    it "render products/show" do
      expect(response).to render_template("potepan/products/show")
    end

    it "return statuscode 200" do
      expect(response.status).to eq(200)
    end

    it "@product should have product" do
      expect(assigns(:product)).to eq(product)
    end

    context "when product.taxons have other product" do
      before do
        bag_taxon.products = [product, bag, tote]
        get :show, params: { id: product.id }
      end

      it "@relation_products have other products that have taxons same @product.taxons " do
        expect(assigns(:relation_products)).to match_array([bag, tote])
      end

      context "when so many products that have same taxon" do
        let(:max_relation_products_in_page_constant) { Potepan::ProductsController::MAX_RELATION_PRODUCTS_IN_PAGE }
        let(:products) { create_list(:product, 5) }

        before do
          bag_taxon.products << products
          get :show, params: { id: product.id }
        end

        it "@relation_products.count <= MAX_RELATION_PRODUCTS_IN_PAGE" do
          expect(assigns(:relation_products).count).to be <= max_relation_products_in_page_constant
        end
      end
    end
  end
end

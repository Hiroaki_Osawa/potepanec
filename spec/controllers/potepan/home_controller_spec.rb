require 'rails_helper'

RSpec.describe Potepan::HomeController, type: :controller do
  let(:two_year_old_product) { create(:product, available_on: 2.year.ago) }
  let(:one_year_old_product) { create(:product, available_on: 1.year.ago) }

  describe "GET #index" do
    before do
      get :index
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "@new_products have products.order(available_on: :desc)" do
      expect(assigns(:new_products)).to match([one_year_old_product, two_year_old_product])
    end

    context "when there are so many products available" do
      let(:max_new_products_in_page) { 10 }
      let!(:products) { create_list(:product, 11) }

      it "@new_products.to_a.size <= MAX_NEW_PRODUCTS_IN_PAGE" do
        expect(assigns(:new_products).to_a.size).to be <= max_new_products_in_page
      end
    end
  end
end

require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  let(:bag_taxon) { create(:taxon, name: "bag") }
  let(:shoes_taxon) { create(:taxon, name: 'shoes') }
  let(:products) { create_list(:product, 5) }
  let(:product) { create(:product) }
  let(:red_option_value) { create(:option_value, name: 'Red') }

  describe "GET categories show with bag_taxon" do
    before do
      get :show, params: { id: bag_taxon.id }
    end

    it "render categories/show" do
      expect(response).to render_template("potepan/categories/show")
    end

    it "return statuscode 200" do
      expect(response.status).to eq(200)
    end

    it "@taxon have bag_taxon" do
      expect(assigns(:taxon)).to eq(bag_taxon)
    end

    it "@products have bag_taxon.products" do
      bag_taxon.products << products
      expect(assigns(:products)).to match_array(products)
    end

    it "@products have not shoes_taxon.products" do
      shoes_taxon.products << products
      expect(assigns(:products)).to match_array([])
    end

    describe "@color_option_values" do
      context "when product  dont have OptionType that have presentation Color and option_value" do
        it "@color_option_values has no one" do
          expect(assigns(:color_option_values)).to eq([])
        end
      end

      context "when product have OptionType that have presentation Color and option_value" do
        let(:bag_color_option_type) { create(:option_type, name: 'bag-color', presentation: 'Color') }

        before do
          bag_taxon.products << product
          bag_color_option_type.option_values << red_option_value
          product.option_types << bag_color_option_type
          get :show, params: { id: bag_taxon.id }
        end

        it "@color_option_values has Color presentation values that belong to @products.option_types" do
          expect(assigns(:color_option_values)).to include(red_option_value)
        end
      end
    end

    context "when params[:color]" do
      let(:red_variant) { create(:variant) }

      before do
        red_variant.option_values << red_option_value
        product.variants << red_variant
        bag_taxon.products << product
        get :show, params: { id: bag_taxon.id, color: red_option_value.name }
      end

      it "@products have variants that have option_values(name: params[:color])" do
        expect(assigns(:products)).to include(red_variant)
      end

      context "when there are multiple variant have same option_value" do
        let(:red_s_size_variant) { create(:variant) }

        before do
          red_s_size_variant.option_values << red_option_value
          product.variants << red_s_size_variant
        end

        it "@products dont have multiple variant belong to same product" do
          expect(assigns(:products).map(&:product_id).count(red_variant.product_id)).to eq(1)
        end
      end
    end

    describe 'product sort methods' do
      let(:ten_price_product) { create(:product, price: 10) }
      let(:twenty_price_product) { create(:product, price: 20) }
      let(:two_year_old_product) { create(:product, available_on: 2.year.ago) }
      let(:one_year_old_product) { create(:product, available_on: 1.year.ago) }

      describe "parmas[:order] based on price" do
        before do
          bag_taxon.products.push(ten_price_product, twenty_price_product)
        end

        context 'when params[:order] == price-asc' do
          it '@product has been sorted price-asc' do
            get :show, params: { id: bag_taxon.id, order: 'price-asc' }
            expect(assigns(:products)).to match([ten_price_product, twenty_price_product])
          end
        end

        context 'when params[:order] == price-dsc' do
          it '@product has been sorted price-dsc' do
            get :show, params: { id: bag_taxon.id, order: 'price-dsc' }
            expect(assigns(:products)).to match([twenty_price_product, ten_price_product])
          end
        end
      end

      describe 'parmas[:order] based on available_on' do
        before do
          bag_taxon.products << [two_year_old_product, one_year_old_product]
        end

        context 'when params[:order] == oldest' do
          it '@products has been sorted oldest' do
            get :show, params: { id: bag_taxon.id, order: 'oldest' }
            expect(assigns(:products)).to match([two_year_old_product, one_year_old_product])
          end
        end

        context 'when params[:order] == newest' do
          it '@products has been sorted newest' do
            get :show, params: { id: bag_taxon.id, order: 'newest' }
            expect(assigns(:products)).to match([one_year_old_product, two_year_old_product])
          end
        end

        context 'when any params[:order]' do
          it '@products has been soreted newest' do
            get :show, params: { id: bag_taxon.id }
            expect(assigns(:products)).to match([one_year_old_product, two_year_old_product])
          end
        end
      end

      context "when params[:color] and params[:order]" do
        let(:red_option_value) { create(:option_value, name: 'Red') }
        let(:ten_price_variant) { create(:master_variant, price: 10) }
        let(:twenty_price_variant) { create(:master_variant, price: 20) }
        let(:two_year_old_variant) { create(:master_variant, product: build(:product, available_on: 2.year.ago)) }
        let(:one_year_old_variant) { create(:master_variant, product: build(:product, available_on: 1.year.ago)) }

        context "when parmas[:order] based on price" do
          before do
            bag_taxon.products.push(ten_price_variant.product, twenty_price_variant.product)
            red_option_value.variants.push(ten_price_variant, twenty_price_variant)
          end

          context 'when params[:order] == price-asc' do
            it '@product has been sorted price-asc' do
              get :show, params: { id: bag_taxon.id, color: 'Red', order: 'price-asc' }
              expect(assigns(:products)).to match([ten_price_variant, twenty_price_variant])
            end
          end

          context 'when params[:order] == price-dsc' do
            it '@product has been sorted price-dsc' do
              get :show, params: { id: bag_taxon.id, color: 'Red', order: 'price-dsc' }
              expect(assigns(:products)).to match([twenty_price_variant, ten_price_variant])
            end
          end
        end

        context 'when parmas[:order] based on available_on' do
          before do
            bag_taxon.products.push(one_year_old_variant.product, two_year_old_variant.product)
            red_option_value.variants.push(one_year_old_variant, two_year_old_variant)
          end

          context 'when params[:order] == oldest' do
            it '@products has been sorted oldest' do
              get :show, params: { id: bag_taxon.id, color: 'Red', order: 'oldest' }
              expect(assigns(:products)).to match([two_year_old_variant, one_year_old_variant])
            end
          end

          context 'when params[:order] == newest' do
            it '@products has been sorted newest' do
              get :show, params: { id: bag_taxon.id, color: 'Red', order: 'newest' }
              expect(assigns(:products)).to match([one_year_old_variant, two_year_old_variant])
            end
          end

          context 'when any params[:order]' do
            it '@products has been soreted newest' do
              get :show, params: { id: bag_taxon.id, color: 'Red' }
              expect(assigns(:products)).to match([one_year_old_variant, two_year_old_variant])
            end
          end
        end
      end
    end
  end
end

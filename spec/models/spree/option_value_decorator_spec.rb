require 'rails_helper'

describe "Spree::OptionValue", type: :model do
  describe "OptionValue.search_with_products_and_presentation" do
    let(:red_option_value) { create(:option_value, name: 'Red') }
    let(:blue_option_value) { create(:option_value, name: 'Blue') }
    let(:shoes_color_option_type) { create(:option_type, name: 'shoes_color', presentation: 'Color') }
    let(:red_shoes_product) { create(:product, name: 'red_shoes') }
    let(:blue_shoes_product) { create(:product, name: 'blue_shoes') }
    let(:products) { Spree::Product.where(name: ['red_shoes', 'blue_shoes']) }

    before do
      shoes_color_option_type.option_values.push(red_option_value, blue_option_value)
      shoes_color_option_type.products.push(red_shoes_product, blue_shoes_product)
      red_shoes_product.master.option_values << red_option_value
      blue_shoes_product.master.option_values << blue_option_value
    end

    it "find option_values belog to OptionType has same product" do
      expect(Spree::OptionValue.search_with_products_and_presentation(products, 'Color')).to include(red_option_value, blue_option_value)
    end
  end
end

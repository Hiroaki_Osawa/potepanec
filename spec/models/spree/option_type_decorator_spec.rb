require 'rails_helper'

describe "Spree::OptionType", type: :model do
  describe "OptionType.search_with_products_and_presentation" do
    let(:bag_color_option_type) { create(:option_type, name: 'bag_color', presentation: 'Color') }
    let(:shoes_color_option_type) { create(:option_type, name: 'shoes_color', presentation: 'Color') }
    let(:bag_product) { create(:product, name: 'bag') }
    let(:shoes_product) { create(:product, name: 'shoes') }
    let(:products) { Spree::Product.where(name: ['bag', 'shoes']) }

    before do
      bag_color_option_type.products << bag_product
      shoes_color_option_type.products << shoes_product
    end

    it "find optiontypes has products and presentation" do
      expect(Spree::OptionType.search_with_products_and_presentation(products, 'Color')).to match_array([bag_color_option_type, shoes_color_option_type])
    end
  end
end

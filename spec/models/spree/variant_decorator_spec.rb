require 'rails_helper'

describe "Spree::Variant", type: :model do
  let(:red_shoes_variant) { create(:variant) }

  describe "Variant.has_option_value" do
    let(:red_option_value) { create(:option_value, name: 'Red') }

    before do
      red_shoes_variant.option_values << red_option_value
    end

    it "search variant has option_value　with name　specified by argument" do
      expect(Spree::Variant.has_option_value(red_option_value.name)).to include(red_shoes_variant)
    end
  end

  describe "Variant.search_with_products" do
    let(:blue_shoes_variant) { create(:variant) }
    let(:red_shoes_product) { create(:product, name: 'red_shoes') }
    let(:blue_shoes_product) { create(:product, name: 'blue_shoes') }
    let(:products) { Spree::Product.where(name: ['red_shoes', 'blue_shoes']) }

    before do
      red_shoes_product.variants << red_shoes_variant
      blue_shoes_product.variants << blue_shoes_variant
    end

    it "find variants belongs to products" do
      expect(Spree::Variant.search_with_products(products)).to include(red_shoes_variant, blue_shoes_variant)
    end
  end

  describe "Variant.order_by" do
    context "when there are variants with different prices" do
      let(:ten_price_variant) { create(:master_variant, price: 10) }
      let(:twenty_price_variant) { create(:master_variant, price: 20) }

      context "when argument == price-dsc" do
        it "variant sorted price dsc" do
          expect(Spree::Variant.order_by('price-dsc')).to match([twenty_price_variant, ten_price_variant])
        end
      end

      context "when argument == price-asc" do
        it "variant sorted price asc" do
          expect(Spree::Variant.order_by('price-asc')).to match([ten_price_variant, twenty_price_variant])
        end
      end
    end

    context "when there are variants belongs to products with different available_on" do
      let(:two_year_old_variant) { create(:master_variant, product: build(:product, available_on: 2.year.ago)) }
      let(:one_year_old_variant) { create(:master_variant, product: build(:product, available_on: 1.year.ago)) }

      context "when argument == oldest" do
        it "variant sorted asc based on product availavele_on" do
          expect(Spree::Variant.order_by('oldest')).to match([two_year_old_variant, one_year_old_variant])
        end
      end

      context "when any argument" do
        it "variant sorted asc based on product availavele_on" do
          expect(Spree::Variant.order_by(nil)).to match([one_year_old_variant, two_year_old_variant])
        end
      end
    end
  end
end

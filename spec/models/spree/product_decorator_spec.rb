require 'rails_helper'

describe "Spree::Product", type: :model do
  describe "Product.taxons_relation_products" do
    let(:ruby_bag_product) { create(:product, name: 'Bag') }
    let(:ruby_tote_product) { create(:product, name: 'Tote') }
    let(:bag_taxon) { create(:taxon, name: 'Bag') }
    let(:ruby_taxon) { create(:taxon, name: 'Ruby') }
    let(:related_products) { Spree::Product.taxons_relation_products(ruby_bag_product) }

    before do
      ruby_bag_product.taxons << bag_taxon
      ruby_bag_product.taxons << ruby_taxon
      ruby_tote_product.taxons << bag_taxon
      ruby_tote_product.taxons << ruby_taxon
    end

    it "find same taxons products" do
      expect(related_products).to include(ruby_tote_product)
    end

    it "return exclude argument" do
      expect(related_products).not_to include(ruby_bag_product)
    end

    it "does not return duplicate records" do
      expect(related_products.ids.count(ruby_tote_product.id)).to eq(1)
    end
  end

  describe "Product.has_option_value" do
    let(:red_product) { create(:product) }
    let(:red_option_value) { create(:option_value) }
    let(:products_with_value) { Spree::Product.has_option_value(red_option_value) }

    before do
      red_product.master.option_values << red_option_value
    end

    it "find same value products" do
      expect(products_with_value).to include(red_product)
    end

    context "when there are product has multiple variants that have same option_value" do
      let(:red_small_variant) { create(:variant) }

      before do
        red_small_variant.option_values << red_option_value
        red_product.variants << red_small_variant
      end

      it "does not return duplicate records" do
        expect(products_with_value.ids.count(red_product.id)).to eq(1)
      end
    end
  end

  describe "Product.reorder_by" do
    context "when argument based on price" do
      let(:product) { create(:product, price: 10) }
      let(:expensive_product) { create(:product, price: 20) }

      context "when argument == price-dsc" do
        it "product sorted price dsc" do
          expect(Spree::Product.reorder_by('price-dsc')).to match([expensive_product, product])
        end
      end

      context "when argument == price-asc" do
        it "product sorted price asc" do
          expect(Spree::Product.reorder_by('price-asc')).to match([product, expensive_product])
        end
      end
    end

    context "when argument based on available_on" do
      let(:two_year_old_product) { create(:product, available_on: 2.year.ago) }
      let(:one_year_old_product) { create(:product, available_on: 1.year.ago) }

      context "when argument == oldest" do
        it "prouct sorted available_on asc" do
          expect(Spree::Product.reorder_by('oldest')).to match([two_year_old_product, one_year_old_product])
        end
      end

      context "when argument == newest" do
        it "product sorted available_on dsc" do
          expect(Spree::Product.reorder_by('newest')).to match([one_year_old_product, two_year_old_product])
        end
      end
    end

    context "when any argument" do
      let(:two_year_old_product) { create(:product, available_on: 2.year.ago) }
      let(:one_year_old_product) { create(:product, available_on: 1.year.ago) }

      it "product sorted available_on dsc" do
        expect(Spree::Product.reorder_by('newest')).to match([one_year_old_product, two_year_old_product])
      end
    end
  end
end

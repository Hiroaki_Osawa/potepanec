require 'rails_helper'

feature "categories pages" do
  given(:bag) { create(:product, name: 'Bag') }
  given(:shoes) { create(:product, name: 'Shoes') }
  given(:tote) { create(:product, name: 'Tote') }
  given(:bag_taxon) { create(:taxon, name: 'Bag') }
  given(:bag_color_option_type) { create(:option_type, name: 'bag-color', presentation: 'Color') }
  given(:red_option_value) { create(:option_value, name: 'Red') }

  background do
    image = File.open(File.expand_path('../../fixtures/thinking-cat.jpg', __FILE__))
    bag.images.create(attachment: image)
    tote.images.create(attachment: image)
    bag_taxon.products.push(bag, tote)
    bag_color_option_type.option_values << red_option_value
    bag.option_types << bag_color_option_type
    visit potepan_category_path(bag_taxon.id)
  end

  scenario "show same category products" do
    expect(page).to have_content 'Bag'
    expect(page).to have_content 'Tote'
  end

  scenario "dont show other taxon products" do
    expect(page).not_to have_content 'Shoes'
  end

  scenario "show color variant name" do
    expect(page).to have_content 'Red'
  end

  scenario "move product page when click product.name link" do
    click_link 'Bag'
    expect(current_path).to eq potepan_product_path(bag.id)
  end

  scenario "default filter pulldown is newest" do
    expect(page).to have_select('order', selected: '新着順')
  end

  scenario "select 価格の安い順 pulldown move potepan_category_path with order query", js: true do
    pending("アセットパイプラインの関係なのかどうしても落ちてしまう")
    select '価格の安い順', from: 'order'
    expect(current_path).to eq potepan_category_path(bag.id, order: 'price-asc')
  end

  scenario "select 価格の安い順 pulldown move 価格の安い順" do
    select '価格の安い順', from: 'order'
    expect(page).to have_select('order', selected: '価格の安い順')
  end

  scenario "select 価格の高い順 pulldown move 価格の高い順" do
    select '価格の高い順', from: 'order'
    expect(page).to have_select('order', selected: '価格の高い順')
  end

  scenario "select 古い順 pulldown move 古い順" do
    select '古い順', from: 'order'
    expect(page).to have_select('order', selected: '古い順')
  end

  context "when visit categories page with color" do
    given(:red_variant) { create(:variant) }

    background do
      red_image = File.open(File.expand_path('../../fixtures/red_thinking-cat.jpg', __FILE__))
      red_variant.images.create(attachment: red_image)
      red_variant.option_values << red_option_value
      bag.variants << red_variant
      visit potepan_category_path(bag_taxon.id, color: 'Red')
    end

    scenario "show same category and have color products" do
      expect(page).to have_content 'Bag'
    end

    scenario "move product page when click product.name link" do
      click_link 'Bag'
      expect(current_path).to eq potepan_product_path(bag.id)
    end

    scenario "show variant image" do
      expect(page).to have_css("img[src*='red_thinking-cat.jpg']")
    end
  end
end
